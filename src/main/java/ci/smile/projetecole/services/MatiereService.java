package ci.smile.projetecole.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ci.smile.projetecole.dao.Matiere;
import ci.smile.projetecole.repositories.MatiereRepository;

@Service
//Matiere entity CRUD services
public class MatiereService {

    @Autowired
    private MatiereRepository matiereRepository;

    //Read
    public List<Matiere> getAllMatiere() {
        return matiereRepository.findAll();
    }

    //Read one
    public Matiere getMatiereById(Long id) {
        return matiereRepository.findById(id).get();
    }

    //Create 
    public Matiere saveMatiere(Matiere matiere) {
        return matiereRepository.save(matiere);
    }

    //Update
    public Matiere updateMatiere(Long matiereId, Matiere matiereToUpdate) {

        Matiere matiere = matiereRepository.findById(matiereId).get();

        matiere.setLibelle(matiereToUpdate.getLibelle());
        matiere.setCoef(matiereToUpdate.getCoef());

        return matiereRepository.save(matiere);
    }

    //Delete
    public void deleteMatiere(Long id) {
        matiereRepository.deleteById(id);
    }
    
}
