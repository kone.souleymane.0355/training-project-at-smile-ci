package ci.smile.projetecole.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ci.smile.projetecole.dao.Note;
import ci.smile.projetecole.repositories.NoteRepository;

@Service
//Note entity CRUD services
public class NoteService {
    
    @Autowired
    //Note reposirory instanciation
    private NoteRepository noteRepository;

    //Read
    public List<Note> getAllNote() {
        return noteRepository.findAll();
    }

    //Read one
    public Note getNoteById(Long id) {
        return noteRepository.findById(id).get();
    }

    //Create
    public Note saveNote(Note note) {
        return noteRepository.save(note);
    }

    //Update
    public Note updateNote(Long noteId, Note noteToUpdate) {

        Note note = noteRepository.findById(noteId).get();

        note.setNote(noteToUpdate.getNote());
        note.setMatiereId(noteToUpdate.getMatiereId());
        note.setEtudiantId(noteToUpdate.getEtudiantId());

        return noteRepository.save(note);
    }

    //Delete
    public void deleteNote(Long id) {
        noteRepository.deleteById(id);
    }
}
