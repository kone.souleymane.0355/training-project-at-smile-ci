package ci.smile.projetecole.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import ci.smile.projetecole.dao.Classe;
import ci.smile.projetecole.dao.Etudiant;
import ci.smile.projetecole.dao.Matiere;
import ci.smile.projetecole.dao.Note;
import ci.smile.projetecole.dto.ClassementDto;
import ci.smile.projetecole.dto.EtudiantDto;
import ci.smile.projetecole.dto.MoyenneDto;
import ci.smile.projetecole.repositories.ClasseRepository;
import ci.smile.projetecole.repositories.EtudiantRepository;

@Service
//Etudiant entity CRUD services
public class EtudiantService {
    
    private final EtudiantRepository etudiantRepository;

    private final ClasseRepository classeRepository;

    public EtudiantService(EtudiantRepository etudiantRepository, ClasseRepository classeRepository) {
        this.etudiantRepository = etudiantRepository;
        this.classeRepository = classeRepository;
    }

    //Read
    public List<Etudiant> getAllEtudiant() {
        return etudiantRepository.findAll();
    }
    //Read
    public Etudiant getEtudiantById(Long id) {
        return etudiantRepository.findById(id).get();
    }

    //Create
    public Etudiant saveEtudiant(EtudiantDto etudiant) {
        Etudiant etudiantToSave = new Etudiant(etudiant.getNom(), 
        etudiant.getPrenom(), etudiant.getDateNaissance(), 
        etudiant.getMatricule(), classeRepository.findById(etudiant.getClasseId()).get());
        return etudiantRepository.save(etudiantToSave);
    }

    //Update
    public Etudiant updateEtudiant(Long etudiantId, Etudiant etudiantToUpdate) {

        Etudiant etudiant = etudiantRepository.findById(etudiantId).get();

        etudiant.setNom(etudiantToUpdate.getNom());
        etudiant.setPrenom(etudiantToUpdate.getPrenom());
        etudiant.setDateNaissance(etudiantToUpdate.getDateNaissance());
        etudiant.setClasseId(etudiantToUpdate.getClasseId());

        return etudiantRepository.save(etudiant);
    }

    //Delete
    public void deleteEtudiant(Long id) {
        etudiantRepository.deleteById(id);
    }

    //Create several etudiant simultaniously
    public List<Etudiant> saveEtudiants(List<EtudiantDto> etudiants) {

        List<Etudiant> etudiantsToSave = etudiants.stream()
                .map(etudiant -> new Etudiant(etudiant.getNom(), 
                etudiant.getPrenom(), etudiant.getDateNaissance(), 
                etudiant.getMatricule(), classeRepository.findById(etudiant.getClasseId()).get()))
                .collect(java.util.stream.Collectors.toList());

        return etudiantRepository.saveAll(etudiantsToSave);
    }

    //Calculate and return Etudiant note average by matiere
    public List<MoyenneDto> getEtudiantNoteAverageByMatiere(String matricule) {

        //Get etudiant from db
        Etudiant etudiant = etudiantRepository.findByMatricule(matricule);
        
        List<MoyenneDto> moyenneList = new ArrayList<>();
        
        //Iterate through matiere to take etudiant note for each matiere
        for(Matiere matiere: etudiant.getClasseId().getMatieres()){

            //variable to calucale average
            double noteMatiere = 0.0;
            int cumulCoef = 0;

            MoyenneDto moyenne = new MoyenneDto();
            
            //Note iterate and average calculate
            for(Note note : etudiant.getNotes()){
                if(note.getMatiereId().getLibelle() == matiere.getLibelle()){
                    noteMatiere = noteMatiere + note.getNote() * matiere.getCoef();
                    cumulCoef = cumulCoef + matiere.getCoef();
                }
            }
            //Put all result together in a dto to send response
            moyenne.setMatiere(matiere.getLibelle());
            moyenne.setMoyenne(noteMatiere/cumulCoef);
            moyenneList.add(moyenne);
        }
        return moyenneList;
    }

    //Calculate and return an etudiant general note average
    public List<ClassementDto> getEtudiantGeneralNoteAverageByClasse() {

        //Get all student from db
        //List<Etudiant> allEtudiant = etudiantRepository.findAll();
        //get all classes from db
        List<Classe> allClasse = classeRepository.findAll();

        //List to store all student note average
        List<ClassementDto> classementList = new ArrayList<>();

        List<MoyenneDto> moyenneList ;

        for(Classe classe : allClasse){

            ClassementDto classementDto = new ClassementDto();
            classementDto.setClasse(classe.getLibelle());

            for(Etudiant etudiant: classe.getEtudiants()){

                EtudiantDto etud = new EtudiantDto();
                double moyenneMatiere = 0.0;
                int counter = 0;

                if(etudiant.getClasseId().getLibelle().equalsIgnoreCase(classe.getLibelle())){

                    moyenneList = getEtudiantNoteAverageByMatiere(etudiant.getMatricule());
                    
                    for(MoyenneDto moy : moyenneList){

                        moyenneMatiere = moyenneMatiere + moy.getMoyenne();
                        counter += 1;
                    }

                    etud.setMatricule(etudiant.getMatricule());
                    etud.setNom(etudiant.getNom());
                    etud.setPrenom(etudiant.getPrenom());
                    etud.setDateNaissance(etudiant.getDateNaissance());
                    etud.setMoyenneGenerale(moyenneMatiere/counter);
                    classementDto.getEtudiant().add(etud);
                }
            }
            classementList.add(classementDto);
        }
        classementList.stream().forEach(classement -> {
            sort(classement.getEtudiant());
            int i = classement.getEtudiant().size();
            for(EtudiantDto etudiant : classement.getEtudiant()){
                etudiant.setRang(i);
                i--;
            }
        });

        return classementList;
    }

    //Sort a list of etudiant by moyenne general
    private void sort(List<EtudiantDto> listEtudiant){

        listEtudiant.sort((etudiant1, etudiant2)
                        -> etudiant1.getMoyenneGenerale().compareTo(
                            etudiant2.getMoyenneGenerale()));
    }
}
