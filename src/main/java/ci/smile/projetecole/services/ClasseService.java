package ci.smile.projetecole.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ci.smile.projetecole.dao.Classe;
import ci.smile.projetecole.repositories.ClasseRepository;

@Service
//Classe entity CRUD services
public class ClasseService {
    
    @Autowired
    //Classe reposirory instanciate
    private ClasseRepository classeRepository;

    //Read
    public List<Classe> getAllClasse() {
        return classeRepository.findAll();
    }

    //Read one
    public Classe getClasseById(Long id) {
        return classeRepository.findById(id).get();
    }

    //Create
    public Classe saveClasse(Classe classe) {
        return classeRepository.save(classe);
    }

    //Update
    public Classe updateClasse(Long classeId, Classe classeToUpdate) {

        Classe classe = classeRepository.findById(classeId).get();

        classe.setLibelle(classeToUpdate.getLibelle());
        classe.setEffectif(classeToUpdate.getEffectif());

        return classeRepository.save(classe);
    }

    //Delete
    public void deleteClasse(Long id) {
        classeRepository.deleteById(id);
    }
}
