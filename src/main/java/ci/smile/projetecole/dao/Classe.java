package ci.smile.projetecole.dao;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Classe extends AbstractEntity {

    private String libelle;

    private String effectif;

    @JsonIgnore
    @OneToMany(mappedBy = "classeId" ,cascade = CascadeType.REMOVE)
    private Set<Etudiant> etudiants;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable
    private Set<Matiere> matieres;

    public Classe(String libelle, String effectif) {
        this.libelle = libelle;
        this.effectif = effectif;
    }
}
