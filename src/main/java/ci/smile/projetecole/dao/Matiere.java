package ci.smile.projetecole.dao;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@NoArgsConstructor
public class Matiere extends AbstractEntity {

    private String libelle;

    private int coef;

    @JsonIgnore
    @OneToMany(mappedBy = "matiereId",cascade = CascadeType.REMOVE)
    private Set<Note> notes;

    @JsonIgnore
    @ManyToMany(mappedBy = "matieres")
    private Set<Classe> classes;

    public Matiere(String libelle, int coef) {
        this.libelle = libelle;
        this.coef = coef;
    }
    
}
