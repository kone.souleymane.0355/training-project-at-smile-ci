package ci.smile.projetecole.dao;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Note extends AbstractEntity {

    private Double note;

    @JsonIgnore
    @ManyToOne
    private Matiere matiereId;

    @JsonIgnore
    @ManyToOne
    private Etudiant etudiantId;

    public Note(Double note, Matiere matiereId, Etudiant etudiantId) {
        this.note = note;
        this.matiereId = matiereId;
        this.etudiantId = etudiantId;
    }
    
}
