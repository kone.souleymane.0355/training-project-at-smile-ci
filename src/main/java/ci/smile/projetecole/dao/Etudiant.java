package ci.smile.projetecole.dao;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Etudiant extends AbstractEntity {

    private String nom;
    
    private String prenom;

    private String dateNaissance;

    private String matricule;

    @JsonIgnore
    @OneToMany(mappedBy = "etudiantId", cascade = CascadeType.REMOVE)
    private Set<Note> notes;

    @JsonIgnore
    @ManyToOne
    private Classe classeId;

    public Etudiant(String nom, String prenom, String dateNaissance, String matricule, Classe classeId) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.matricule = matricule;
        this.classeId = classeId;
    }
}
