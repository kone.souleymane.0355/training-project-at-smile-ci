package ci.smile.projetecole.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class EtudiantDto {

    private String nom;
    
    private String prenom;

    private String dateNaissance;

    private String matricule;

    private Double moyenneGenerale = 0.0;
    
    private int rang;
    
    private Long classeId;
}
