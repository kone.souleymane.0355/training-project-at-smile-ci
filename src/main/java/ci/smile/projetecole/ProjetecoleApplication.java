package ci.smile.projetecole;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetecoleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetecoleApplication.class, args);
	}

}
