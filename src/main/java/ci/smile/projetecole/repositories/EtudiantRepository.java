package ci.smile.projetecole.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ci.smile.projetecole.dao.Etudiant;

@Repository
//Etudiant entity Repository
public interface EtudiantRepository extends JpaRepository<Etudiant, Long> {

    //Select an etudiant by his matricule
    Etudiant findByMatricule(String matricule);
}
