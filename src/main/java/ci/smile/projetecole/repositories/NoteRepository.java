package ci.smile.projetecole.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ci.smile.projetecole.dao.Note;

@Repository
//Note entity Repository
public interface NoteRepository extends JpaRepository<Note, Long> {

}
