package ci.smile.projetecole.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ci.smile.projetecole.dao.Matiere;

@Repository
//Matiere entity Repository
public interface MatiereRepository extends JpaRepository<Matiere, Long> {
    
}
