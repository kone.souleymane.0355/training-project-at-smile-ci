package ci.smile.projetecole.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ci.smile.projetecole.dao.Classe;

@Repository
//Classe entity Repository
public interface ClasseRepository extends JpaRepository<Classe, Long> {

}

