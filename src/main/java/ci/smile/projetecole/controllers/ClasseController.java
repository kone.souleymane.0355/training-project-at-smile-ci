package ci.smile.projetecole.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ci.smile.projetecole.dao.Classe;
import ci.smile.projetecole.services.ClasseService;
import lombok.Data;

@RestController
@Data
@RequestMapping("/api/v1/classes")
public class ClasseController {
    
    @Autowired
    private ClasseService classeService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(classeService.getAllClasse());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(classeService.getClasseById(id));
    }

    @PostMapping
    public ResponseEntity<?> createClasse(@RequestBody Classe classe) {
        return ResponseEntity.ok(classeService.saveClasse(classe));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateClasse(@PathVariable("id") Long id, @RequestBody Classe classeToUpdate) {
        return ResponseEntity.ok(classeService.updateClasse(id, classeToUpdate));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteClasse(@PathVariable("id") Long id) {
        classeService.deleteClasse(id);
        return ResponseEntity.ok("Ok");
    }
}
