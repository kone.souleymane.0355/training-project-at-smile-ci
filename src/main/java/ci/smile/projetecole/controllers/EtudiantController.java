package ci.smile.projetecole.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ci.smile.projetecole.dao.Etudiant;
import ci.smile.projetecole.dto.EtudiantDto;
import ci.smile.projetecole.services.EtudiantService;
import lombok.Data;

@RestController
@Data
@RequestMapping("/api/v1/etudiants")
public class EtudiantController {

    @Autowired
    private EtudiantService etudiantService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(etudiantService.getAllEtudiant());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(etudiantService.getEtudiantById(id));
    }

    @PostMapping
    public ResponseEntity<?> createEtudiant(@RequestBody EtudiantDto etudiant) {
        return ResponseEntity.ok(etudiantService.saveEtudiant(etudiant));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateEtudiant(@PathVariable("id") Long id, @RequestBody Etudiant etudiantToUpdate) {
        return ResponseEntity.ok(etudiantService.updateEtudiant(id, etudiantToUpdate));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEtudiant(@PathVariable("id") Long id) {
        etudiantService.deleteEtudiant(id);
        return ResponseEntity.ok("Ok");
    }

    @PostMapping("/saveall")
    public ResponseEntity<?> saveAll(@RequestBody List<EtudiantDto> etudiants) {
        return ResponseEntity.ok(etudiantService.saveEtudiants(etudiants));
    }

    @GetMapping("/average/{matricule}")
    public ResponseEntity<?> getAverage(@PathVariable("matricule") String matricule) {
        return ResponseEntity.ok(etudiantService.getEtudiantNoteAverageByMatiere(matricule));
    }

    @GetMapping("/average")
    public ResponseEntity<?> getGeneralAverage() {
        return ResponseEntity.ok(etudiantService.getEtudiantGeneralNoteAverageByClasse());
    }
    
}
