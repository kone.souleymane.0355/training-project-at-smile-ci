package ci.smile.projetecole.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ci.smile.projetecole.dao.Matiere;
import ci.smile.projetecole.services.MatiereService;
import lombok.Data;

@Data
@RestController
@RequestMapping("/api/v1/matieres")
public class MatiereController {

    @Autowired
    private MatiereService matiereService;

    @GetMapping
    public ResponseEntity<?> getAllMatiere() {
        return ResponseEntity.ok(matiereService.getAllMatiere());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getMatiereById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(matiereService.getMatiereById(id));
    }

    @PostMapping
    public ResponseEntity<?> saveMatiere(@RequestBody Matiere matiere) {
        return ResponseEntity.ok(matiereService.saveMatiere(matiere));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateMatiere(@PathVariable("id") Long id, @RequestBody Matiere matiereToUpdate) {
        return ResponseEntity.ok(matiereService.updateMatiere(id, matiereToUpdate));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMatiere(@PathVariable("id") Long id) {
        matiereService.deleteMatiere(id);
        return ResponseEntity.ok().build();
    }
    
}
